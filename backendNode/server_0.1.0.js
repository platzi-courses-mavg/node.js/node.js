// import express from 'express'; // Nueva sintaxis de EcmaScript 6 para traerse módulos de otro sitio
const express = require('express'); // Forma de Node para traese módulos de cualquier otro sitio
// const bodyParser = require('body-parser'); // BodyParser ya esta deprecado
const router = express.Router();
const response = require('./network/response');

var app = express(); // Express inicilaizado
// app.use(bodyParser.json()); // BodyParser ya esta deprecado
// app.use(bodyParser.urlencoded({ extendend: false })); // BodyParser ya esta deprecado
app.use(express.json());
app.use(router);

// // app.use('/', (req, res) => {  // Nueva sintaxis de EcmaScript 6 con Arrow functions (Funciones flecha)
// app.use('/', function(req, res) { // Antigua sintaxis 
// res.send('Hola');
// });


// ---
// http://localhost:3000/message
router.get('/message', function(req, res) {
  // console.log("HEADERS:", req.headers);
  res.header({
    "custom-header": "Valor personalizado"
  })
  // res.send('Lista de mensajes');
  // response.success(req, res);
  response.success(req, res, 'Lista de mensajes');
});


// ---
// http://localhost:3000/message
router.delete('/message', function(req, res) {
  res.send('Mensaje eliminado');
});


// ---
// http://localhost:3000/message?orderBy=id&age=15
router.post('/message', function(req, res) {
  // console.log("BODY:", req.body);
  // console.log("QUERY:", req.query);
  // res.send('Mensaje "' + req.body.text + '" añadido correctamente');
  // res.status(201).send()
  // res.send({ error: '', body: 'Creado correctamente' });
  if (req.query.error == 'ok')
    // response.error(req, res, 'Error simulado', 400);
    response.error(req, res, 'Error inesperado', 500, 'Es solo una simulación de los errores');
  else
    response.success(req, res, 'Creado correctamente', 201);
})

app.use('/app', express.static('public'))

app.listen(3000);
console.log('La aplicación esta escuchando en: http://localhost:3000/');