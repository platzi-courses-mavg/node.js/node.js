const express = require('express');

// const router = require('./components/message/network');
const router = require('./network/routes');
const db = require('./db')

const uri = "mongodb+srv://platzi-admin:JLw0rFda01Sw0MYE@cursodeplatzi.xhmpk.mongodb.net" // process.env.PLATZI_DB_URI
db(uri);

var app = express();
app.use(express.json());
// app.use(router);

router(app);

app.use('/app', express.static('public'))

app.listen(3000);
console.log('La aplicación esta escuchando en: http://localhost:3000/');