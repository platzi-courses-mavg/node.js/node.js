const store = require('./store');

// ---
// GET 
// function getMessages() {
//   return new Promise((resolve, reject) => {
//     resolve(store.list());
//   })
// }

function getMessages(filterUser) {
  return new Promise((resolve, reject) => {
    resolve(store.list(filterUser));
  })
}

// ---
// POST
function addMessage(user, message) {
  return new Promise((resolve, reject) => {
    if (!user || ! message) {
      console.error('[messageController] No hay usuario o mensaje');
      reject('Los datos son incorrectos');
    }

    const fullMessage = {
      user,
      message,
      date: new Date()
    };
  
    store.add(fullMessage);

    resolve(fullMessage);
  })
}

// ---
// PATCH
function updateMessage(id, message) {
  return new Promise(async (resolve, reject) => {
    if (!id || !message) {
      reject('Invalid data');
      return false;
    }
    
    const result = await resolve(store.updateMessage(id, message));
    resolve(result);

  })
}

// ---
// DELETE
function removeMessage(id) {
  return new Promise((resolve, reject) => {
    if (!id) {
      reject('Id Invalido');
      return false;
    }

    store.removeMessage(id)
      .then(() => {
        resolve();
      })
      .catch(e => {
        reject(e);
      })
  });
}

module.exports = {
  addMessage,
  getMessages,
  updateMessage,
  removeMessage
};