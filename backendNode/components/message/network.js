const express = require('express');

const response = require('../../network/response');
const controller = require('./controller');

const router = express.Router();

// ---
// GET | http://localhost:3000/message
// router.get('/', function(req, res) {
//   controller.getMessages()
//     .then((messageList) => {
//       response.success(req, res, messageList, 200);
//     })
//     .catch(e => {
//       response.error(req, res, 'Unexpected Error', 500, e)
//     })
// });

router.get('/', function(req, res) {
  const filterMessages = req.query.user || null;

  controller.getMessages(filterMessages)
    .then((messageList) => {
      response.success(req, res, messageList, 200);
    })
    .catch(e => {
      response.error(req, res, 'Unexpected Error', 500, e)
    })
});


// ---
// POST | http://localhost:3000/message
router.post('/', function(req, res) {
  controller.addMessage(req.body.user, req.body.message)
    .then((fullMessage) => {
      // response.success(req, res, 'Creado correctamente', 201);
      response.success(req, res, fullMessage, 201);
    })
    .catch(e => {
      response.error(req, res, 'Información Invalida', 400, 'Error en el controlador. ' + e);
    })
})

// ---
// PATCH | http://localhost:3000/message/id
router.patch('/:id', function(req, res) { // Verbo HTTP para hacer modificaciones parciales
  controller.updateMessage(req.params.id, req.body.message)
    .then((data) => {
      response.success(req, res, data, 200);
    })
    .catch(e => {
      response.error(req, res, 'Error interno', 500, 'Error en el controlador. ' + e);
    })
})

// ---
// DELETE | http://localhost:3000/message/id
router.delete('/:id', function(req, res) {
  controller.removeMessage(req.params.id)
    .then(() => {
      response.success(req, res, `Mensaje ${req.params.id} eliminado`, 200);
    })
    .catch(e => {
      response.error(req, res, 'Error interno', 500, `Error en el controlador. ${e}`);
    })
})

module.exports = router;  