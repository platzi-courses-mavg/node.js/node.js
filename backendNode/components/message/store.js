const Model = require('./model');

// ---
// GET
// async function getMessages() {
//   const messages = await Model.find();  
//   return messages;
// }

async function getMessages(filterUser) {
  return new Promise((resolve, reject) => {
    let filter = {};

    if (filterUser !== null) 
      filter = { user: filterUser }

    Model.find(filter)
      .populate('user')
      .exec((error, populated) => {
        if (error) {
          reject(error)
          return false
        }

        resolve(populated)
      });
  });
}

// ---
// POST
function addMessage(message) {
  const myMessage = new Model(message);
  myMessage.save();
}

// ---
// PATCH
async function updateMessage(id, message) {
  const foundMessage = await Model.findOne({
    _id: id
  });

  foundMessage.message = message;
  const newMessage = await foundMessage.save();
  return newMessage;
}

// ---
// DELETE
function removeMessage(id) {
  return Model.deleteOne({
    _id: id
  });
}

module.exports = {
  add: addMessage,
  list: getMessages,
  updateMessage,
  removeMessage
}