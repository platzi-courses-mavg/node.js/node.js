const store = require('./store');

// ---
// GET 
function getChats() {
  return store.getChats(userId);
}

// ---
// POST
function addChat(users) {
  if (!users || !Array.isArray(users))
    return Promise.reject('Invalid user list');

  const chat = { users, };

  return store.add(chat);
}

module.exports = {
  addChat,
  getChats,
};