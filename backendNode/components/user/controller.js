const store = require('./store');

// ---
// GET 
function getUsers() {
  return store.getUsers();
}

// ---
// POST
function addUser(name) {
  if (!name)
    return Promise.reject();

  const user = { name, };

  return store.addUser(user);
}

// ---
// PATCH
function updateUser(id, name) {
  if (!id || !name) 
    return Promise.reject();
  
  return store.updateUser(id, name);
}

// ---
// DELETE
function removeUser(id) {
  return new Promise((resolve, reject) => {
    if (!id) {
      reject('Id Invalido');
      return false;
    }

    store.removeUser(id)
      .then(() => { resolve(); })
      .catch(e => { reject(e); })
  });
}

module.exports = {
  addUser,
  getUsers,
  updateUser,
  removeUser
};