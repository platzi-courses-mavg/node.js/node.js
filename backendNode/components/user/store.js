const Model = require('./model');

// ---
// GET
function getUsers() {
  return Model.find();  
}

// ---
// POST
function addUser(user) {
  const myUser = new Model(user);
  return myUser.save();
}

// ---
// PATCH
async function updateUser(id, name) {
  const foundUser = await Model.findOne({ _id: id });
  foundUser.name = name;
  return foundUser.save();
}

// ---
// DELETE
function removeUser(id) {
  return Model.deleteOne({ _id: id });
}

module.exports = {
  addUser,
  getUsers,
  updateUser,
  removeUser
}