const express = require('express');

const response = require('../../network/response');
const controller = require('./controller');

const router = express.Router();

// ---
// GET | http://localhost:3000/user
router.get('/', function(req, res) {
  controller.getUsers()
    .then((userList) => { response.success(req, res, userList, 200); })
    .catch(e => { response.error(req, res, 'Unexpected Error', 500, e) })
});

// ---
// POST | http://localhost:3000/user
router.post('/', function(req, res) {
  controller.addUser(req.body.name)
    .then(data => { response.success(req, res, data, 201); })
    .catch(e => { response.error(req, res, 'Información Invalida', 400, 'Error en el controlador. ' + e); })
})

// ---
// PATCH | http://localhost:3000/user/id
router.patch('/:id', function(req, res) { // Verbo HTTP para hacer modificaciones parciales
  controller.updateUser(req.params.id, req.body.name)
    .then((data) => { response.success(req, res, data, 200); })
    .catch(e => { response.error(req, res, 'Error interno', 500, 'Error en el controlador. ' + e); })
})

// ---
// DELETE | http://localhost:3000/user/id
router.delete('/:id', function(req, res) {
  controller.removeUser(req.params.id)
    .then(() => { response.success(req, res, `Usuario ${req.params.id} eliminado`, 200); })
    .catch(e => { response.error(req, res, 'Error interno', 500, `Error en el controlador. ${e}`); })
})

module.exports = router;  