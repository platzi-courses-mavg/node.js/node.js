# Node.js <a name="inicio"/>

## Contenido
+ 1. [¿Qué es Node?](#Node)
+ 2. [Peticiones HTTP](#Peticiones)
  + 2.1 [Métodos y Cabeceras](#MetodosCabeceras)
  + 2.2 [Cuerpo y Query de la Petición](#CuerpoQuery)
+ 3. [Creación de un Servidor HTTP](#Server)
  + 3.1 [Inicialización del Proyecto](#Init)
  + 3.2 [Creación del servidor](#CreacionServidor)
  + 3.3 [Tipos de respuesta](#TiposRespuestas)
  + 3.4 [Server Estáticos](#Estaticos)
+ 4. [Arquitectura básica de un backend](#Arquitectura)
  + 4.1 [Separación conceptual de rutas, controladores y bases de datos](#SeparacionConceptual)
+ 5. [Bases de datos](#DB)
+ 6. [Entidades para crear aplicaciones escalables](#Entidades)


---
## ¿Qué es Node y cómo instalarlo? <a name="Node"/>[↑](#inicio)
***Node.js*** es un entorno en tiempo de ejecución multiplataforma de código abierto para la capa del servidor basado en el lenguaje de programación *ECMAScript*  asíncrono y basado en el *Motor V8* de Google.
Simplemente es ejecutar *JavaScript* en un entorno totalmente diferente al navegador, por ejemplo, en una terminal o en un sevidor.

Node no solo se puede utilizar para crear servidores, sino también para crear herramientas, por ejemplo:
* [*Webpack*](https://github.com/webpack/webpack), que se utilza para compilar todos los "archivos sueltos" en único archivo para "servir";
* [*Babel*](https://babeljs.io/), utilizado para convertir JavaScript nuevo en código antiguo para navegadores viejos;
* [*PM2*](https://pm2.keymetrics.io/), para crear servidores, sobretodo de alto tráfico, en producción;
* [*Electron*](https://www.electronjs.org/), convierte cualquier aplicación web en una aplicación de escritorio.

Para insatalar el entorno basta con visitar la [página oficial](https://nodejs.org/en/) y descargar e instalar la *Versión LTS*.


---
## Peticiones HTTP <a name="Peticiones"/>[↑](#inicio)
Una petición HTTP o el Protocolo HTTP, es el protocolo de comunicación que permite tranferir información entre cualquier elemento que este conectado a la Web, servidores, máquinas, clientes, puntos de IoT; prácticamente cualquier elemento que se comunique por internet. Es la base de cualquier intercambio de datos en la Web, y un protocolo de estructura *cliente-servidor*, esto quiere decir que una petición de datos es iniciada por el elemento que recibirá los datos (el cliente), normalmente un navegador Web. Así, una página web completa resulta de la unión de distintos sub-documentos recibidos.

Es importante porque es el lenguaje común para todas las comunicaciones.

Una ***petición*** se puede definir como el "pedido" que hace un *cliente* hacia internet, donde desde aquí, este pedido es enviado al *servidor* que se encarga de procesarlo para después devolverlo al cliente.

Ejemplo de una *petición* y una *respuesta*:

<table>
  <tr>
    <th>Petición</th>
    <th>Respuesta</th>
  </tr>
  <tr>
    <td>
      <code>
        GET /index.html HTTP/1.1
        Host: www.example.com
        Referer: www.google.com
        User-Agent: Mozilla/5.0
        Connection: keep-alive
      </code>
    </td>
    <td>
      <code>
        HTTP/1.1 200 OK
        <b>Date:</b> Fri, 31 Jun 2019 23:59:59 GMT
        Content-Type: text/html
        Content-Length: 1221
      </code>
    </td>
  </tr>
</table>

**Puntos clave de una petición HTTP:** *Método*, *Estado* y *Cuerpo*.

### Métodos y Cabeceras <a name="MetodosCabeceras"/>[↑](#inicio)
#### Métodos
Es el verbo que indicará al servidor "lo que queremos hacer". De esta manera indicaremos si queremos obtener información, editarla, insertarla, etc.

Los verbos principales que podemos utilizar son:

Verbo | Función
------|------------
***GET*** | Recoger información del servidor, ya sea la información de un producto, un listado de elementos, ver una página HTML o un archivo CSS, etc.
***POST*** | Añadir información al servidor, como, añadir un producto nuevo, o enviar un formulario.
***PUT*** | Remplazar o editar información en el servidor, por ejemplo, cambiar el contenido de una página, remplazar algún producto, editar un mensaje, etc.
***DELETE*** | Eliminar completamente información del servidor, ya sea un mensaje, un producto del carrito, etc.
***PATCH*** | Actualizar solo una parte de la información, como para cambiar la foto de un usuario o modificar el precio de un producto.
***OPTIONS*** | Pedir información sobre los métodos, para saber si podemos ejecutar en alguna dirección el método POST, PUT, DELETE, o PATCH.

#### Cabeceras
Nos dan información contextual de la petición, es decir, NO se especifica *"que es lo que quiero hacer"* sino *"como lo quiero hacer"*.

El tipo de información que se puede mandar es, por ejemplo, en peticiones POST, PUT y PATCH las cabeceras para *AUTENTICACIÓN*, *CACHE*, *INDICACIONES*, *CONDICIONES*, *CORS*, *COOKIES*, etc.

***COOKIES*** simplemente funcionan para compartir información entre peticiones, es una cabecera que se encarga de compartir información entre las peticiones y las respuestas del cliente y el servidor, por ejemplo, para tener conciencia todo el tiempo de cierto usuario. Esta petición se mantiene durante toda la comunicación.

***CORS*** *Cors Origin Resource Sharing* que es compartir recursos entre diferentes orígenes, permitiendo y no el manejo de información desde fuera de nuestro servidor. La cabecera *Access-Control-Allow-Origin* nos permite definir desde donde se pueden consumir las peticiones a nuestro servidor.

***ACCEPT*** define el tipo de contenido que acepta el servidor, por ejemplo, solo contenido en *JSON* o en *HTML*, o solo en el *charset UTF-8*, o solo codificado de cierta manera.

***AUUTHORIZATION*** utilizada para autenticación y para asegurar que se pueden hacer peticiones al servidor, si no se cuenta con esta autorización, el servidor no devolvería la información solicitada.

***CACHÉ*** almacenamiento temporal; esto le dice al cliente durante cuanto tiempo la respuesta va a ser la misma, por ejemplo, si frecuentemente utilizo una foto, se puede decir al servidor que no este realizando la petición cada que sea necesario utilizar la foto, sino que mejor la guarde en cache, de esta manera se logra que el servidor tenga menos peticiones y más recursos para cosas de verdad importantes. Las cabeceras más utilizadas son *Cache-Control* y *Expires*.

#### Estados
Son un número que indican lo que ha pasado con la petición, si fue bien o mal, o si se ha redirigió.

Su estructura siempre comienza con un número, que "agrupa" los estados y se acompaña por dos números más, por ejemplo ***2XX***. Ejemplos:

Estado | Descripción
-------|------------
***2XX*** | **Todo ha ido bien**
*200* | OK. Todo en orden
*201* | Created. Se ha creado el recurso
***3XX*** | **La petición se ha redirigido a otro sitio**
*301* | Moved permanently. El recurso se movio permanentemente de una url a otra, o de un servidor a otro
*304* | Not modified. No ha habido ningún tipo de modificacion en el recurso
***4XX*** | **Errores del cliente**
*400* | Bad request. El cliente ha enviado algo mal
*401* | Unauthorized. El cliente no tiene permiso para obtener respuesta de esta petición. El servidor no sabe quien es el cliente
*403* | Forbidden. Prohibido, el servidor sabe quien es el cliente pero no puede darle la información solicitada
*404* | Not found. No se ha encontrado el recurso que se esta pidiendo
***5XX*** | **Errores del servidor**
*500* | Internal server error. Ha habido un problema interno y no se puede brindar más información al cliente

### Cuerpo y Query de la Petición <a name="CuerpoQuery"/>[↑](#inicio)
#### Body
El ***Cuerpo (Body)*** de la petición es la información en si, que se desea añadir o enviar al servidor, por ejemplo, si queremos insertar un usuario, entonces se hará una petición *POST* y en el *cuerpo de la petición* se añadirá la información del usuario, como el nombre, el correo, la contraseña, etc. 

El tipo de datos, como HTML o datos crudos, que se "envían" en la petición depende de las cabeceras:
* ***Content-Type*** que indica exactamente el tipo de contenido obtenido, por ejemplo, *text/html*, *text/css*, *application/javascript*, *image/jpeg*, *application/json* o *application/xml* y 
* ***Content-Length*** que informa la "longitud" del contenido.

En la respuesta podremos recibir cualquier método HTTP; un archivo html, css, js, etc.; los datos de algún producto; etc.

**Ejemplo de request POST para añadir un usuario a la Base de Datos:**

    [POST]
    http://api.com/user
      -> Content-Type: application/json

    {
      "name": "Miguel",
      "email": "mvaldes988@gmail.com",
      "pass": "1a2b3c"
    }

    
**Ejemplo del response:**

    [POST]
    http://api.com/user
      -> Content-Type: application/json

    {
      "id": "d64ca236-0b37-47a8-85bd-450cc04b637e"
      "name": "Miguel",
      "email": "mvaldes988@gmail.com",
    }

#### Query
Las queries permiten añadir información extra a lo que se quiera enviar al servidor, por ejemplo, el orden de los parámetros en la respuesta, si esperamos un dato en específico, o si queremos medir algo en especial.

Esta información extra se añade en la URL de la petición siguiendo la siguiente estructura.
* Seguido de la URL añadir el carácter "***?***",
* Añadir las propiedades en una estructura "***nombre=valor***" y
* Para añadir más de un parámetro se debe agregar el carácter "***&***" entre cada parámetro.

Por ejemplo:
* youtube.com/watch?**v=ZKFwOFBwQFU**
* api.com/person?**orderBy=name&age=25**

Los queries también se pueden utilizar para compartir datos con el Frontend, pero se debe tener cuidado con estos datos, porque el usuario siempre podrá verlos, nunca se debe enviar información sensible, como, tokens de usuario o información de tarjetas de credito. Esta información debe ser envíada a través del *Body* de la petición.

Ejemplos de información compartida con el Frontend:
* miweb.com?**utm_source=medium**
* miweb.com?**color=red**

En resumen, los **parámetros querie** se pueden utilizar para mejorar la comunicación entre cliente y servidor, para compartir información o hacer peticiones muy específicas y toda la información se puede agregar en el **body de la request**.


---
## Creación de un Servidor HTTP <a name="Server"/>[↑](#inicio)
### Inicialización del proyecto <a name="Init"/>[↑](#inicio)
Al instalar Node, automaticamente se instala ***NPM***, el ***Node Package Manager*** es lo que nos permite gestionar y trabajar con paquetes Node. Los paquetes, simplemente son lo que en cualquier otro lenguaje sería una librería, son trocitos de código encapsulados o empaquetados con el fin de ser utilizados en diferentes partes del desarrollo.

Para utilizar NPM en cualquier proyecto Node, basta con correr desde terminal el comando, `npm init`, al hacerlo se pedirá que registremos algunos datos como:
* *package name*. Nombre del Proyecto, por defecto se selecciona el nombre de la carpeta en la que se corre el comando `npm init`.
* *version*. Version del proyecto, automaticamente aparece la 1.0.0. Es recomendado utilizar la 0.1.0 y pasar a la version 1.0.0 en cuanto el desarrollo este listo para pasar a producción.
* *description*. Aquí se puede agregar una breve descripción del proyecto.
* *entry point*. Punto de entrada hacía el paquete (proyecto), es el primer archivo *JavaScript* que se ejecuta para desplegar todo el código creado. Por defecto sugiere *index.js*.
* *test command*. Comando para realizar pruebas en el paquete.
* *git repository*. Repositorio Git remoto del paquete.
* *keywords*. Palabras clave del paquete. Útiles para mejorar la búsqueda del paquete. Mientras el proyecto este en un repositorio privado, no habrá problema, pero si se pública en el NPM, se pueden utilizar estas keyboards para encontrar el módulo en el buscador.
* *author*. Desarrollador del proyecto.
* *license*. Licencias con las que cuente el paquete.

Una vez registrada esta información se crea el archivo *package.json*, donde se muestra toda la información del proyecto incluidos los paquetes (dependencias) utilizadas.

Para instalar nuevos paquetes en el proyecto basta con correr el comando `npm install 'nombreDelPaquete'` o `npm i 'nombreDelPaquete'`, por ejemplo, `npm i express`.

#### Nodemon
Es un paquete que permite actualizar automáticamente el servidor al guardar los cambios del código, evitando así que cada que se haga un cambio en el código del servidor sea necesario "tumbar" y "levantar" el proceso. Para instalar el paquete basta con correr el comando `npm i -g nodemon`. El *modificador* `-g` instala el paquete de manera global, es decir, en todo el sistema.

[Código](./backendNode/package.json)

### Creación del servidor <a name="CreacionServidor"/>[↑](#inicio)
Para crear un servidor en NodeJS es necesario seguir los siguientes puntos:
+ Instalar, importar y utilizar la librería ***express***,
+ Inicializar *express*, declarando una `variable = express()`, 
+ Aplicar el método `use` con sus dos parámetros, la ***ruta o el endpoint*** y un ***Callback*** que a su vez recibe los parámetros ***req*** (request) y ***res*** (response), y
+ Aplicar el método `listen` asignandole un puerto para escuchar las peticiones que entrarán al servidor.

#### Router
El método ***router*** de ***express*** permite separar peticiones por *cabeceras*, *métodos* o *url*, dentro del servidor.

#### Body parser
Es un módulo, o extensión, de ***express*** que permite trabajar con el *body* (cuerpo) de las peticiones de forma más sencilla. (`npm i body-parser`). **NOTA: Hoy en día, este módulo ya esta deprecado, en su lugar se utiliza *express.json()***

Del parámetro ***req*** del servidor, se pueden obtener el *body (cuerpo)*, el *query* y las *header (cabeceras)* de la petición. 

En las *cabeceras* se incluye información como; *user-agent* lugar o navegador desde donde se realiza la petición, el *host* lugar al que se realiza la petición, el *content-length*, la *connection*, el *cache-control*, el *accept-encoding* para determinar si se quiere o espera la información codificada, el *accept-language* idioma en el que se espera la información, las *cookie* por defecto, etc.

De estas cabeceras, las más relevantes son: *cache-control*, útil para settear cache específico para imágenes, archivos de JavaScript, etc., y el *user-agent* ayuda a determinar si las consultas se realizan desde mobiles, o desde cierto sistema operativo.

En la respuesta (***res***) también podemos incluir *header* específicos, por ejemplo, `res.header({ "custom-header": "Valor personalizado" })`, con el fin de enviarlas al cliente y poderlas utilizar en aplicaciones como determinar información útil para analiticas (ads), o para hacer *cache-control* o *max-age*.

[Código](./backendNode/server_0.1.0.js)

### Tipos de respuesta <a name="TiposRespuestas"/>[↑](#inicio)
Pueden ser:
* *vacía*, `res.send();`, respuesta sin ningún tipo de cuerpo,
* *plana*, `res.send('Lista de mensajes');`,
* *con datos*, `res.send('Mensaje "' + req.body.text + '" añadido correctamente');` y
* *estructurada*, una forma habitual de responder es devolviendo un objeto con una serie de información, por ejemplo, `res.send({ error: '', body: 'Creado correctamente' });`, incluso se puede devolver un array de objectos.

**Cualquier tipo de respuesta puede enviar un status, por ejemplo, `res.status(201).send()`.**

En resumen se puede responder cualquier tipo de respuesta en las peticiones HTTP, trabajando con *código de estatus*, *respuestas vacías*, un simple *mensaje plano*, u objetos y arreglos complejos siguiendo una *data* o *estructura*.

[Código](./backendNode/server_0.1.0.js)

#### Respuestas coherentes.
Es buena práctica crear un [módulo encargado de responder](./backendNode/network/response.js) todas las peticiones de forma fácil de entender siguiendo una estructura definida. Por ejemplo:
    
    exports.success = (req, res, message, status) => {
      res.status(status || 200).send({
        error: '',
        body: message
      })
    }

    exports.error = (req, res, message, status) => {
      res.status(status || 500).send({
        error: message,
        body: ''
      })
    }

#### Errores: Cómo presentarlos e implicaciones en la seguridad.
Es muy importante tener cuidado con la información que se envía al cliente, por ejemplo para el caso de un Login, si el cliente ingresa correctamente el usuario pero la contraseña es incorrecta y como respuesta se envía que el usuario es correcto pero la contraseña no, entonces se estaría dando información de la existencia del usuario en la plataforma y a partir de ahí podrían hacer peticiones de *generación de servicio* o de *fuerza bruta* para obtener la contraseña, o incluso *peticiones de ingeniería social* para obtener todo ese tipo de datos. Si se desea devolver un error que sea coherente, dando algo de información al usuario, pero de parte del servidor saber exactamente que es lo que esta pasando, entonces se pueden utilizar los logs para conocer los detalles del error, permitiendo así tener un log especificando que es lo que ha sucedido y un mensaje sencillo para el usuario, que no siempre tendrían que ser el mismo. Jamas se debe responder con errores o con información confidencial de los errores al cliente.

[Código](./backendNode/network/response.js)

### Server Estáticos <a name="Estaticos"/>[↑](#inicio)
El *servidor de estáticos de express* es muy útil para servir archivos estáticos para el Frontend de la aplicación, por ejemplo archivos *HTML*, *CSS*, *JavaScript*, etc.

Para utilizarlo basta con agregar una ruta y utilizar el método `static()` de express en ella. Ejemplo: `app.use('/app', express.static('public'))`. Una buena práctica es usar la carpeta *"public"* para guardar ahí toda la información publica de la aplicación.

[Código](./backendNode/public)


---
## Arquitectura básica de un backend <a name="Arquitectura"/>[↑](#inicio)
### Separación conceptual de rutas, controladores y bases de datos <a name="SeparacionConceptual"/>[↑](#inicio)
Para planear la arquitectura de una aplicación se debe separar conceptualmente las *rutas*, los *controladores* y el *almacenamiento (Base de datos)*. 

Lo primero a revisar es el origen de las peticiones, el primero en recibirlas es el ***server.js*** y se encarga de comprobar que las peticiones son correctas para procesarlas en el servidor o directamente cancelarlas si hubiera algún tipo de problema o fallo, también se encarga de configurar toda la información importante del servidor, base de datos, cabeceras, etc. ***server.js*** envía la información al "controlador" que gestiona las rutas, el ***routes.js***, revisando hacía donde quiere ir la petición para llamar al componente adecuado, estos componentes (***components***) son los ***modules*** del sistema y cada uno tiene toda la información y lógica relacionada con el tema represetado por el nombre del modulo. 
Dentro de cada modulo o componente se puede tener:
* un archivo de rutas (***network.js***), desde el cuál se gestionan todas las rutas, es decir todos los endpoint y la información relacionada con el protocolo *HTTP*, 
* un ***controller.js*** que contiene toda la *lógica de negocio* del componente, es decir, el procesamiento del modulo, ya sea crear o editar alguna entidad, hacer comprobaciones, etc. y
* un ***store.js*** encargado de gestionar la *base de datos*, es decir, específica donde y como se guarda la información. Una vez realizadas todas las comprobaciones en el controlador, es probable que sea necesario realizar algún registro de lo procesado en una base de datos.

Esta estructura en los *components* facilita la escalación de la aplicación, ya que todo, tanto la lógica, como el almacenamiento y la red, esta separado conceptualmente.

Para mantener una estructura igual en las respuestas de la aplicación, aún para los diferentes *components*, se puede utilizar el ***response.js***, el cuál recibirá la respuesta del *network.js* de cada componente en lugar de que el *network.js* envíe la respuesta al *router.js*, de esta forma, *response.js* será el que siempre responda al cliente final, asegurando así, mantener siempre una consistencia en las formas de las peticiones y las respuestas.

![Rutas, Controladores y Base de datos](./images/Rutas-Controladores-y-BaseDeDatos.PNG "Rutas, Controladores y Base de datos")

De esta manera, observando conceptualmente, se tienen tres capas:
* Capa principal, **capa de servidor**, donde se tiene toda la información del servidor y la configuración,
* **Capa de red** y
* **Capa de componentes**.

De esta forma, todo lo que se realice será cien por ciento escalable, permitiendo también crecer la arquitectura hasta donde se desee.


La ***Capa de Red*** es la encargada de recibir las peticiones HTTP, procesar toda la información y enviarla al controlador.

Para desarrollar la *capa de red* es necesario comenzar a separar las utilidades o funcionalidades de la aplicación en [componentes](./backendNode/components). Cada componente (definido en una carpeta) debe tener un archivo con sus rutas ([network.js](./backendNode/components/message/network.js), que contenga toda la parte de red y se encargue de traducir la petición del cliente HTTP a la acción que se desee realizar en el controlador.

Para manejar más de un *router.js* (en caso de que se tenga más de un *componente*), se puede tener toda la información de los *router.js* en un archivo separado ([routes.js](./backendNode/network/routes.js)), donde se separarían las rutas por los componentes.

Una vez creado el archivo *routes.js*, se debe requerir en el *server.js* para que el servidor se encargue de crear todas las rutas necesarias.


El ***Controller*** es se encarga de definir todo lo que sucede creando las funciones necesarias.

En los *controladores* es importante trabajar con *Promesas* (o similar) para con estás notificar a la *capa de red* si algo ha salido bien o mal durante el procesamiento de la petición. Aquí es donde se suele validar que la información agregada en la petición sea correcta. Trabajando con promesas es importante agregar algún *catch*, para así, en caso de que algo falle, se tenga algún error con su causa.


La única resposabilidad de la ***Capa de Almacenamiento*** es manejar toda la lógica de almacenamiento de datos en la base de datos.


Si se debe realizar una modificación dentro de la *capa de almacenamiento* (funciones de la Base de Datos), no será necesario hacer ningún tipo de cambio en los *controladores*; esto es uno de los beneficios de separar fisicamente el código.


Un ***Mock*** es falsear la base de datos, un servicio o cualquier cosa a utilizar, para validar el correcto funcionamiento; en otras palabras, es un objeto que simula o imita el comportamiento de uno real con el fin de poder hacer testing sin la necesidad de, por ejemplo, tener una capa compleja de base de datos.


---
## Bases de datos <a name="DB"/>[↑](#inicio)
Las bases de datos más tipicas y más comunes habían sido siempre *MySQL* esto gracias a su uso en todas las aplicaciones debido a su "feed" perfecto con *PHP* (uno de los lenguages más utilizados para Backend). *MySQL* es una *base de datos relacional* que se basa en tablas al igual que por ejemplo *Postgre*.

### Bases de Datos Relacionales.
Se basan en tablas donde básicamente cada tabla representa un distinto tipo de información. En las tablas podemos crear distintas columnas para los distintos datos y cada una de las filas representarán un registro diferente. Por ejemplo podríamos tener una base de datos de "Personas", donde una de sus tablas es "Persona" y dentro de sus columnas (campos) podríamos tener "Nombre", "Apellido", "Edad", etc. y cada persona añadida será una nueva fila, es decir un nuevo registro. De la misma manera se podría tener una tabla de "Mensajes" y cada mensaje (cada registro de la tabla) podría especificar el nombre de la persona que lo ha creado, para esto, se crea la *relación* mediante un *identificador* único, NO se puede crear la relación a partir del nombre del usuario (o de cualquier otro campo) ya que este podría cambiar. De esta manera, cada fila (o cada registro) tendrá un identificador único, y al momento de crear un mensaje y "referirlo" a un usuario, ese identificador será utilizado como clave externa. Los motores de las bases de datos relacionales se encargan de crear las relaciones de estas claves "por detrás" para poder traer toda la información relacionada.

En aplicaciones grandes como Facebook, esto no es escalable, debido a que la capacidad de computo necesaria se eleva demasiado cuando, por ejemplo, se tengan que hacer consultas muy complejas que requieran información de muchos tipos y sitios diferentes; además es necesario que estás consultas se ejecuten en tiempo real, entonces, a partir de necesidades como está, es donde aparecen las *Base de Datos NoSQL o No Relacionales*.

### Base de Datos NoSQL o No Relacionales.
Ahora es más común hablar de ellas. Son llamadas "NoSQL" porque el lenguage de consulta para las bases de datos relacionales es SQL, para este tipo de bases de datos no necesariamente se utiliza SQL, cabe mencionar que existen algunas en las que se puede utilizar una versión reducida de SQL para hacer las consultas, pero normalmente este tipo de bases de datos tienen sus propios lenguages y sus propias librerías de consulta.

#### Tipo de Bases de Datos No Relacionales.
Existen bastantes tipos de bases de datos no relaciones, a continuación se mencionan los más comunes:
* **de Documentos**. Trabajan a base de documentos donde se determina el formato o la estructura del mismo (normalmente en formato JSON). No crean relaciones.
* **de Documentos Relacionados**. Son bases de datos de documentos que permiten tener relaciones, internamente se encarga de generar esas relaciones en tiempo de ejecución. Este tipo de Base de datos, por ejemplo *Mongo*, se utilizan demasiado, debido a su potencia y a su facilidad de ordenar muy bien los datos.
* **de Clave Valor**. Simplemente, sus registros se representan por una clave, que funciona como identificador único, y un valor que figura lo que se desee guardar. Un ejemplo de esta base de datos es *Cassandra*.
* **de Grafo**. Básicamente guardan información como nodos y crea relaciones entre ellos. Estás bases de datos son muy utiles cuando se almacena información del estilo de una red social o para análisis de información muy complejos, se utilizan para detectar patrones extraños en envíos de emails, patrones de comportamiento inusuales, detectar fraude bancario, etc. Un ejemplo de estás bases de datos es *Neo4j*.

![Tipos de Base de datos](./images/Tipos-de-BasesDeDatos.JPG "Tipos de Base de datos")

### MongoDB
Es un sistema de Bases de datos No Relacional, se utiliza mucho cuando se esta prototipando un proyecto, o cuando se quiere lanzar un producto que puede cambiar de manera rápida. Es una base de datos que no tiene esquemas permitiendo así definirlos desde software, es decir moldear el tipo que tienen, sus validaciones y actualizaciones en cualquier momento. Pemite tener muchos documentos y cambiar el esquema (por algún fuerte cambio en la aplicación) sin problemas, simplemente se actualiza el esquema y todo sigue funcionando sin ningún problema.

Para realizar la conexión a un servicio externo de MongoDB se puede utilizar *Mongo Atlas* para conectarse a una instancia de MongoDB, para esto se puede hacer uso de las *variables de entorno*, así, por cada *environment* se puede utilizar diferentes configuraciones, es decir, en Desarrollo se podría realizar la conexión a una instancia de Mongo Atlas o hacer la conexión a una instancia local, en Pruebas, se deberían utilizar diferentes bases de datos.

Una *URI* de MongoDB tiene la sigueinte estructura: *mongodb+srv://DB_USER:DB_PASSWORD@DB_HOST/DB_NAME*, donde necesita el usuario de la base de datos, la contraseña, el host y el nombre. Todo esto es lo que se puede representar en las *variables de entorno* para al momento de cambiar de ambiente, sea muy fácil de reemplazar.

***MongoDB Atlas*** es un servicio de bases de datos que permite configurar bases de datos para entornos de pruebas o producción con diferentes proveedores de nube. Además de todas sus características, Atlas se destaca por ser un servicio mantenido oficialmente por el equipo que desarrolla MongoDB.

***mLab*** también era un servicio de bases de datos con Mongo. Pero fue adquirido por MongoDB a inicios de 2019 para "impulsar" a MongoDB Atlas y unir fuerzas con el anterior equipo de mLab.

Un ***clúster fragmentado*** en MongoDB es una colección de conjuntos de datos distribuidos en muchos fragmentos (servidores) para lograr una escalabilidad horizontal y un mejor rendimiento en las operaciones de lectura y escritura. La fragmentación es muy útil para colecciones que tienen una gran cantidad de datos y altas tasas de consulta.

Al crear un clúster en MongoDB, se debe contemplar el Acceso de Red por Base de Datos. Mongo Atlas limita el acceso a las bases de datos. Para dar acceso a ellas se debe obtener la IP de la máquina que establecerá la conexión y así poder dar el permiso, desde Mongo Atlas, solo a esa IP. **Es buena práctica restringuir las conexiones a las bases de datos por IP**. También es necesario crear un usuario para poder establecer la conexión.
El clúster creado se compone de tres instancias de MongoDB, lo que permite tener alta disponibilidad a diferencia de otros servicios.

Una vez creado el clúster se debe crear la Base de Datos desde el apartado *"Collections"* y seleccionando el botón *"Add my own data"*

Para conectar el clúster a nuestra aplicación basta con entrar al apartado *"Connect"* y seleccionar una de las opciones donde aparecerá la URI a utilizar.

*Mongo DB* no tiene esquemas, pero NO es buena práctica trabajar sin ningún tipo de esquema, ya que sería complicado determinar el tipo de información almacenada en la base de datos. Es importante saber que almacenamos, como lo almacenamos y porque lo almacenamos. ***Mongoose*** es una libreria Object Data Modeling (ODM) para *MongoDB* y *Node.js* que permite crear y modificar esquemas desde software (desde código), para la base de datos, también se encarga de hacer la validación de datos, evitando así guardar datos incorrectos y a su vez previniendo ataques o fallas de consistencia dentro de la información y la base de datos.

Para crear un *esquema* en *mongoose* es necesario:
* Crear una constante *mongoose.Schema* (`const Schema = mongoose.Schema`),
* Crear una constante instanciando el *Schema* creado anteriormente, donde a través de un objeto se pueden definir todos los campos del esquema, así como sus propiedades y tipos. Ej:
      const mySchema = new Schema({
        user: String, 
        message: {
          type: String,
          required: true
        },
        ...
      })

Para utilizar un *esquema* dentro del software, se debe crear un ***model***, el cuál se encarga de "crear todo" a partir del esquema. Para crear el *model* son necesarios dos parámetros: el nombre de la colección y el esquema (`const model = mongoose.model('Message', mySchema)`), este modelo es el que se utilizará siempre que se quiera hacer cualquier tipo de modificación sobre la base de datos, exclusivamente sobre lo relacionado a mensajes.

Para generar la conexión a la Base de Datos, basta con crear una instancia de la dependencia *mongoose* y utilizar el método `connect()` enviando como parametros la *URI* del clúster de MongoDB, y de ser necesario (o como buena práctica), las configuraciones para entablar la conexión (`db.connect(uri, 'confs')`).

Como buena práctica se pueden utilizar promesas para cerciorarse de la correcta conexión con la Base de Datos:

```
const db = require('mongoose');

db.Promise = global.Promise;

db.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('[db] Conectada con éxito'))
  .catch(err => console.error('[db]', err));
```

[Código](./backendNode)


---
## Entidades para crear aplicaciones escalables <a name="Entidades"/>[↑](#inicio)
Al desarrollar, es muy importante seguir una arquitectura. Mediante un buen diseño de componentes se puede lograr una escalación del proyecto muy sencilla debido a que la aplicación se vuelve muy *modular*, y la creación de entidades se simplifica considerablemente.

Relacionar entidades es sencillo utilizando los *Schema de mongoose*. Para esto es necesario hacer uso del tipo especial de Mongo, el tipo ***ObjectId*** en las propiedades del campo en la definición de su esquema, además se de de agregar el "identificador" del otro esquema a referenciar. 

```
const mySchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  message: ...
});
```

Sin embargo, esta referencia no muestra *populada* la segunda entidad, ***popular*** no es más que, si lo que se tiene es una referencia a un objeto, en lugar de mostrar esa referencia, se muestra la información del objeto obteniendolo a partir de la base de datos. Para *popular* la información basta con implementar el método `populate()` que busca los elementos de tipo *ObjectId* dentro de la colección de Mongo y los *popula*. Es necesario ejecutar el *populado* con el método `exec()` ya que no se ejecuta de manera automática debido a que es una operación propia y algo compleja, consiste en buscar los *ObjectId* dentro de las entidades y después pasa a la entidad a la que pertence ese *ObjectId* para buscarlo dentro de la misma, para así, al final regresar la información de esta segunda entidad en conjunto con la primera

```
Model.find(filter)
  .populate('user')
  .exec((error, populated) => {
    if (error) {
      reject(error)
      return false
    }

    resolve(populated)
  });
```

[Código](./backendNode/components/message/store.js)

